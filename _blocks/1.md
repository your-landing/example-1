---
block: bigcenter
display: 100 # 25, 50, 75, 100, auto
background: 'assets/placeholder.png'
color: light
suptitle: Template for
title: Your landing
description: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro doloremque facilis, cum est tenetur dicta excepturi maxime nihil voluptatibus dolores? Reiciendis incidunt laboriosam possimus ipsam, eum, hic repudiandae soluta dicta.
buttons:
  - name: Learn more
    url: 'https://gitlab.com/your-landing/example'
    color: primary
    outline: false
  - name: Demo
    url: 'https://your-landing.gitlab.io/example-1/'
    color: light
    outline: true
---
